package main;

import java.util.logging.*;
import java.util.logging.Logger;
import org.junit.Test;

public class BankApplication {

	@Test
	public void log() {
		Handler handler = new ConsoleHandler();
		Logger base = Logger.getLogger("org.jdom");
		base.addHandler(handler);
		Logger attr = Logger.getLogger("org.jdom.Attribute");
		Logger elt = Logger.getLogger("org.jdom.Element1");
		attr.addHandler(handler);
		// base == elt.getParent()

		elt.info("elt.info Displayed");
		attr.info("attr.info Displayed");
		base.setLevel(Level.SEVERE);
		elt.info("Level.SEVERE: elt.info Hidden");
		attr.info("Level.SEVERE: attr.info Hidden");
		elt.setLevel(Level.INFO);
		elt.info("Level.INFO: elt.info Displayed");
		elt.fine("Level.INFO: elt.fine Fine");
		attr.severe("Level.INFO: attr.severe Severe");

	}
}
